# McFalls TTT Server

To run the server, use the `start_ttt.sh` script.

    usage: start_ttt <steam_api_key>

You _must_ run this from the repo root because the config files are mounted relatively.

You may create a wrapper script called start_ttt_wrapper.sh that is ignored so that you do not need to find your API key each time. You can also set the working directory so you can call this wrapper from anywhere.

## Workshop

This server using this [steam workshop collection](steam://openurl/https://steamcommunity.com/sharedfiles/filedetails/?id=2070971747). No guarantees that it will work with a different wokshop collection.

## Data

Game install and addon data will be mounted to the `data` directory in the repo root.

## Config files

The following config files are mounted

* `ttt_server.cfg`

This is the main server config file.

* `ulx_config.txt`

This is the ULX config, which includes things enabling the MOTD and other commands.

* `motd.txt`

This is the MOTD file used by ULX.

* `users.txt`

This is the users file for ULX. This allows TechnoSam to be added as an admin automatically.

* `hitboxcfg.dat`

This is the data file used by the [Hitbox Fixer addon](https://steamcommunity.com/sharedfiles/filedetails/?id=2186933267). All the models that don't have proper hitboxes need this otherwise they won't be headshot-able.

* `mapvote_config.txt`

This is the config file used by the [MapVote addon](https://steamcommunity.com/sharedfiles/filedetails/?id=151583504). We use the prefixes as a whitelist. This seems to work, but I think that some maps may not be getting included.
