if [ "$#" -ne 1 ]; then
    echo "usage: start_ttt <steam_api_key>"
    exit
fi

docker run -it \
    -p 27015:27015/tcp \
    -p 27015:27015/udp \
    -e "AUTHKEY=$1" \
    -e "GAMEMODE=terrortown" \
    -e "MAP=ttt_community_bowling_v5a" \
    -e "WORKSHOP=3015056195" \
    -e "WORKSHOPDL=3015056195" \
    -v `pwd`/ttt_server.cfg:/opt/steam/garrysmod/cfg/server.cfg \
    -v `pwd`/ulx_config.txt:/opt/steam/garrysmod/data/ulx/config.txt \
    -v `pwd`/motd.txt:/opt/steam/garrysmod/data/ulx/motd.txt \
    -v `pwd`/users.txt:/opt/steam/garrysmod/data/ulib/users.txt \
    -v `pwd`/hitboxcfg.dat:/opt/steam/garrysmod/data/hitboxcfg.dat \
    -v `pwd`/mapvote_config.txt:/opt/steam/garrysmod/data/mapvote/config.txt \
    -v `pwd`/recentmaps_override.txt:/opt/steam/garrysmod/data/mapvote/recentmaps.txt:ro \
    -v `pwd`/data:/opt/steam \
    hackebein/garrysmod
